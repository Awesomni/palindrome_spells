"""Модуль для нахождения палиграмм и палиндромов."""
import sys
# import cProfile


def load_dict(file_path):
    """Открывает файл и возвращает список строк в нижнем регистре."""
    try:
        with open(file_path) as in_file:
            return [x.lower() for x in in_file.read().strip().split('\n')]
    except IOError as err:
        print(f'{err}\nОшибка при открытии {file_path}. Программа завершается', file=sys.stderr)


def find_palingrams(word_list=None) -> list:
    """Отыскивает палинграммы в словарею."""
    pali_list = []
    if word_list is None:
        word_list = load_dict('/usr/share/dict/web2')
    word_list = set(word_list)
    for word in word_list:
        end = len(word)
        rev_word = word[::-1]
        if end > 1:
            for i in range(end):
                if word[i:] == rev_word[:end - i] and rev_word[end - i:] in word_list:
                    pali_list.append((word, rev_word[end - i:]))
                if word[:i] == rev_word[end - i:] and rev_word[:end - i] in word_list:
                    pali_list.append((word, rev_word[:end - i]))

    return pali_list


def main():
    """Главная функция."""
    words = load_dict('/usr/share/dict/web2')
    pali_list = [word for word in words if len(word) > 1 and word == word[::-1]]
    print(len(pali_list))
    for idx, pali in enumerate(pali_list, 1):
        print(f'{idx:<3}{pali:>15}')

    print('\n' * 3)

    # cProfile.run('find_palingrams()')

    palingrams = sorted(find_palingrams(words))
    print(f'Число палинграмм: {len(palingrams)}')
    for idx, pali in enumerate(palingrams, 1):
        print(f'{idx:<3}{pali[0]:>15} {pali[1]:<10}')


if __name__ == '__main__':
    main()
